//
//  Checklogin.swift
//  RAC-final
//
//  Created by MSU IT CS on 11/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class Checklogin:Mappable{
    
    var token:String?
    var info:ModelUserInfo?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        token <- map["token"]
        info <- map["user_info"]
        
    }
    typealias WebServiceResponse = ([Checklogin]?,Error?) -> ()
    typealias Response = (Checklogin?,Error?) -> ()
    
    class func check(_ url:URL,username:String,password:String,completion: @escaping Response)
    {
        var params = [String:AnyObject]()
        params["username"] = username as AnyObject
        params["password"] = password as AnyObject
        
        var user = [String:AnyObject]()
        user["session"] = params as AnyObject
        
        Alamofire.request(url, method: .post, parameters: user, headers: nil).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<Checklogin>().map(JSONObject: json as AnyObject)
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
    
    
}
