//
//  UserDefault.swift
//  Form
//
//  Created by MSU IT CS on 22/4/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

enum UserDefaultkeys: String {
    case token
    case username
    case name
    case email
    case position
    case tel
    case image
    case role
    case user_id
    case gender
    case address
    case verify
    case latitude
    case longtitude
}

extension UserDefaults{
    
    func logout(){
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.token.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.username.rawValue)
        UserDefaults.standard.removeObject(forKey:  UserDefaultkeys.name.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.email.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.position.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.tel.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.image.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.role.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.user_id.rawValue)
        
        
    }
    func delelocation(){
        
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.latitude.rawValue)
        UserDefaults.standard.removeObject(forKey: UserDefaultkeys.longtitude.rawValue)

    }
    func savelocation(latitude:String?,longtitude:String?){
        set(latitude , forKey:UserDefaultkeys.latitude.rawValue)
        set(longtitude, forKey:UserDefaultkeys.longtitude.rawValue)
    }
    func getlatitude() -> String? {
        
        return string(forKey: UserDefaultkeys.latitude.rawValue)
    }
    func getlongtitude() -> String? {
        
        return string(forKey: UserDefaultkeys.longtitude.rawValue)
    }
    func savetoken(token:String){
        set(token, forKey: UserDefaultkeys.token.rawValue)
    }
    func gettoken() -> String? {
        
        return string(forKey: UserDefaultkeys.token.rawValue)
    }
    
    func saveinfo(username:String?,name:String?,email:String?,position:String?,tel:String?,image:String?,role:Int?,user_id:Int?,gender:String?,address:String?,verify:String?){
        set(username, forKey: UserDefaultkeys.username.rawValue)
        set(name, forKey: UserDefaultkeys.name.rawValue)
        set(email, forKey: UserDefaultkeys.email.rawValue)
        set(position, forKey: UserDefaultkeys.position.rawValue)
        set(tel, forKey: UserDefaultkeys.tel.rawValue)
        set(image, forKey: UserDefaultkeys.image.rawValue)
        set(role, forKey: UserDefaultkeys.role.rawValue)
        set(user_id, forKey: UserDefaultkeys.user_id.rawValue)
        set(gender, forKey: UserDefaultkeys.gender.rawValue)
        set(address, forKey: UserDefaultkeys.address.rawValue)
        set(verify, forKey: UserDefaultkeys.verify.rawValue)
        
    }
    func getverify() -> String? {
        
        return string(forKey: UserDefaultkeys.verify.rawValue)
    }
    func getaddress() -> String? {
        
        return string(forKey: UserDefaultkeys.address.rawValue)
    }
    func getgender() -> String? {
        
        return string(forKey: UserDefaultkeys.gender.rawValue)
    }
    func getusername() -> String? {
        
        return string(forKey: UserDefaultkeys.username.rawValue)
    }
    func getname() -> String? {
        return string(forKey: UserDefaultkeys.name.rawValue)
        
    }
    func getemail() -> String? {
        return string(forKey: UserDefaultkeys.email.rawValue)
        
    }
    func getposition() -> String? {
        return string(forKey: UserDefaultkeys.position.rawValue)
        
    }
    func gettel() -> String? {
        return string(forKey: UserDefaultkeys.tel.rawValue)
        
    }
    func getimage() -> String? {
        return string(forKey: UserDefaultkeys.image.rawValue)
        
    }
    func getrole() -> String? {
        return string(forKey: UserDefaultkeys.role.rawValue)
        
    }
    func getuser_id() -> Int? {
        return integer(forKey: UserDefaultkeys.user_id.rawValue)
        
    }
    
}
