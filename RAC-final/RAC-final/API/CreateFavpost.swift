//
//  Favpost.swift
//  RAC-final
//
//  Created by MSU IT CS on 16/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class CreateFavpost:Mappable{
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([CreateFavpost]?,Error?) -> ()
    typealias Response = (CreateFavpost?,Error?) -> ()
    
    
    class func create(_ url:URL,token:String?,post_id:Int?,completion: @escaping Response)
    {
        let params : [String:AnyObject] = [
            "favoritepost" :[
                "post_id":post_id
         ] as AnyObject
        ]
        
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<CreateFavpost>().map(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
}

