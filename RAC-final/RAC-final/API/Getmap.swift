//
//  Getmap.swift
//  RAC-final
//
//  Created by MSU IT CS on 15/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class Getmap:Mappable{
    
  
    var location:ModelLocation?
    var id:Int?
    var user_id:String?
    var title:String?
    var description:String?
    var type_post:String?
    var status:String?
    var name:String?
    var images = [ModelImagepost]()
    var creater:ModelCreaterpost?
    var updated_at:String?
    var nature:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
       location <- map["location"]
        id <- map["id"]
        user_id <- map["user_id"]
        title <- map["title"]
        description <- map["description"]
        type_post <- map["type_post"]
        status <- map["status"]
        name <- map["name"]
        images <- map["images"]
        updated_at <- map["updated_at"]
        creater <- map["creater"]
        nature <- map["nature"]

    }
    
    typealias WebServiceResponse = ([Getmap]?,Error?) -> ()
    typealias Response = (Getmap?,Error?) -> ()
    
    class func check(_ url:URL,token:String?,offset:Int?,completion: @escaping WebServiceResponse)
    {
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        var params = [String:AnyObject]()
        
        if offset != nil{
            params["offset"] = offset as AnyObject
            params["limit"] = 5 as AnyObject
        }
     
        Alamofire.request(url, method: .get, parameters: params, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
               let array = Mapper<Getmap>().mapArray(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
}
