//
//  GetUserinfo.swift
//  RAC-final
//
//  Created by MSU IT CS on 19/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class GetUserinfo:Mappable{
    
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var phone:String?
    var gender:String?
    var address:String?
    var image:String?
    var role:Int?
    var user_id:Int?
    var verify:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        username <- map["username"]
        name <- map["name"]
        email <- map["email"]
        position <- map["position"]
        address <- map["address"]
        gender <- map["gender"]
        phone <- map["phone"]
        image <- map["image"]
        role <- map["role_id"]
        user_id <- map["id"]
        verify <- map["verify"]
    }
    
    
    typealias WebServiceResponse = ([GetUserinfo]?,Error?) -> ()
    typealias Response = (GetUserinfo?,Error?) -> ()
    
    class func get(_ url:URL,token:String,completion: @escaping Response)
    {
        let headers: HTTPHeaders = [
            "token": token,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .get, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetUserinfo>().map(JSONObject: json as AnyObject)
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
}
