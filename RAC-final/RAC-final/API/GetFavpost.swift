//
//  GetFavpost.swift
//  RAC-final
//
//  Created by MSU IT CS on 17/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class GetFavpost:Mappable{
    
    
    var id:Int?
    var user_id:Int?
    var updated_at:String?
    var post:ModelFavpost?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        post <- map["post"]
        updated_at <- map["updated_at"]
    }
    
    typealias WebServiceResponse = ([GetFavpost]?,Error?) -> ()
    typealias Response = (GetFavpost?,Error?) -> ()
    
    class func get(_ url:URL,token:String?,completion: @escaping WebServiceResponse)
    {
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .get, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetFavpost>().mapArray(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
}
