//
//  Createpost.swift
//  RAC-final
//
//  Created by MSU IT CS on 15/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class Createpost:Mappable{
    
    var message:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        message <- map["message"]
    }
    
    typealias WebServiceResponse = ([Createpost]?,Error?) -> ()
    typealias Response = (Createpost?,Error?) -> ()
    
    
    class func create(_ url:URL,token:String?,title: String,description: String
        ,type_post: String,nature:String,identify:Bool,image :[String],titlelocation:String,latitude:String,longtitude:String,completion: @escaping Response)
    {

        let Post: [String:AnyObject] = [
            "post":[
                "title":title,
                "description":description,
                "nature":nature,
                "type_post" :type_post,
                "status" :"Wait",
               "image" : image,
               "identify":identify
                ] as AnyObject,
            "location":[
                "title" :titlelocation,
                "latitude" :latitude,
                "longitude" :longtitude
            ] as AnyObject
        ]
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: Post, headers: headers).validate().responseJSON { response in
            switch response.result {
                
            case .success(let json):
                
                let array = Mapper<Createpost>().map(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
}
