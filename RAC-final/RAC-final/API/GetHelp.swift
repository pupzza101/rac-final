//
//  GetHelp.swift
//  RAC-final
//
//  Created by MSU IT CS on 18/7/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetHelp:Mappable{
    
    var title:String?
    var description:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    typealias WebServiceResponse = ([GetHelp]?,Error?) -> ()
    typealias Response = (GetHelp?,Error?) -> ()
    
    class func get(_ url:URL,token:String?,completion: @escaping WebServiceResponse)
    {
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .get, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetHelp>().mapArray(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
    
}
