//
//  DeleteFavpost.swift
//  RAC-final
//
//  Created by MSU IT CS on 16/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class DeleteFavpost:Mappable{
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([DeleteFavpost]?,Error?) -> ()
    typealias Response = (DeleteFavpost?,Error?) -> ()
    
    
    class func delete(_ url:URL,token:String?,completion: @escaping Response)
    {
        
        
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .delete, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<DeleteFavpost>().map(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
}
