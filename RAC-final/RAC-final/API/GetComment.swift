//
//  File.swift
//  RAC-final
//
//  Created by MSU IT CS on 17/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class GetComment:Mappable{
    
    var id:Int?
    var post_id:Int?
    var user_id:Int?
    var description:String?
    var name:String?
    var image_user:String?
    var role_id:Int?
    var updated_at:String?
    var image:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        post_id <- map["post_id"]
        user_id <- map["user_id"]
        description <- map["description"]
        name <- map["name"]
        image <- map["image"]
        image_user <- map["image_user"]
        role_id <- map["role_id"]
        updated_at <- map["updated_at"]
    }
    
    typealias WebServiceResponse = ([GetComment]?,Error?) -> ()
    typealias Response = (GetComment?,Error?) -> ()
    
    
    class func get(_ url:URL,token:String?,completion: @escaping WebServiceResponse)
    {
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .get, parameters: nil, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<GetComment>().mapArray(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
}
