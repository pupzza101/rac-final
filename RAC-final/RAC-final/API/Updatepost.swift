//
//  Updatepost.swift
//  RAC-final
//
//  Created by MSU IT CS on 18/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class Updatepost:Mappable{
    
    
     required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    typealias WebServiceResponse = ([Updatepost]?,Error?) -> ()
    typealias Response = (Updatepost?,Error?) -> ()
    
    
    class func update(_ url:URL,token:String?,title: String?,description: String?
        ,status:String? ,type_post: String?,nature:String?,image :[String]?,titlelocation:String?,latitude:String?,longtitude:String?,completion: @escaping WebServiceResponse)
    {
       let Post: [String:AnyObject]
       
             Post = [
                "post":[
                    "title":title,
                    "description":description,
                    "nature":nature,
                    "type_post" :type_post,
                     "status":status,
                    "image" : image
                    ] as AnyObject,
                "location":[
                    "title" :titlelocation,
                    "latitude" :latitude,
                    "longitude" :longtitude
                    ] as AnyObject
            ]
      
      
        
        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .put, parameters: Post, headers: headers).validate().responseJSON { response in
            switch response.result {
                
            case .success(let json):
                
                let array = Mapper<Updatepost>().mapArray(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
}
