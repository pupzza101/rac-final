//
//  CreateComment.swift
//  RAC-final
//
//  Created by MSU IT CS on 18/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class CreateComment:Mappable{
    
    var message:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        message <- map["message"]
    }
    
    typealias WebServiceResponse = ([CreateComment]?,Error?) -> ()
    typealias Response = (CreateComment?,Error?) -> ()
    
    class func create(_ url:URL,token:String?,post_id:Int?,description:String?,image:String?,completion: @escaping Response)
    {
        var params = [String:AnyObject]()
           // "comment"
               // "post_id":post_id,
              //  "description":description
                params["post_id"] = post_id as AnyObject
                params["description"] = description as AnyObject

                //"image":image
       
        if image != nil{
            params["image"] = image as AnyObject

        }
        
        var comment = [String:AnyObject]()
        comment["comment"] = params as AnyObject

        let headers: HTTPHeaders = [
            "token": token!,
            //  "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: comment, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let json):
                
                let array = Mapper<CreateComment>().map(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
}
