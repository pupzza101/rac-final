//
//  Register.swift
//  RAC-final
//
//  Created by MSU IT CS on 13/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Alamofire
import ObjectMapper

class Register:Mappable{
    
    var message:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        message <- map["message"]
        
    }
    
    typealias WebServiceResponse = ([Register]?,Error?) -> ()
    typealias Response = (Register?,Error?) -> ()
    
    class func create(_ url:URL,username: String,password: String
        ,name: String,email: String,tel:String,image:String?,gender:String,address:String,completion: @escaping Response)
    {
        var params = [String:AnyObject]()
        params["username"] = username as AnyObject
        params["password"] = password as AnyObject
        params["name"] = name as AnyObject
        params["email"] = email as AnyObject
        params["phone"] = tel as AnyObject
        params["address"] = address as AnyObject
        params["gender"] = gender as AnyObject
        params["position"] = "" as AnyObject
        params["image"] = image as AnyObject
        params["role_id"] = 1 as AnyObject
        var user = [String:AnyObject]()
        user["user"] = params as AnyObject
        
        let headers: HTTPHeaders = [
            "type": "base64",
            //  "Content-Type": "application/json"
        ]
        Alamofire.request(url, method: .post, parameters: user, headers: headers).validate().responseJSON { response in
            switch response.result {
                
            case .success(let json):
                
                let array = Mapper<Register>().map(JSONObject: json as! [String:Any])
                //mapArray(JSONObject: json as AnyObject)!
                
                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    
    
    
    
}
