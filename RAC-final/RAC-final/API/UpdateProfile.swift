//
//  UpdateProfile.swift
//  RAC-final
//
//  Created by MSU IT CS on 19/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class UpdateProfile:Mappable{
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    
    typealias WebServiceResponse = ([UpdateProfile]?,Error?) -> ()
    typealias Response = (UpdateProfile?,Error?) -> ()
    
    
    class func update(_ url:URL,user_id:Int ,name: String,email: String,tel:String,image:String?,gender:String,address:String,verify:Int?,verify_image:String?,completion: @escaping Response)
    {
        var params = [String:AnyObject]()
    
        params["name"] = name as AnyObject
        params["email"] = email as AnyObject
        params["phone"] = tel as AnyObject
        params["address"] = address as AnyObject
        params["gender"] = gender as AnyObject
        params["position"] = "" as AnyObject
        params["role_id"] = 1 as AnyObject
       
        if verify != nil{
            params["verify"] = verify as AnyObject
            params["verify_image"] = verify_image as AnyObject
        }else{
            params["image"] = image as AnyObject

        }
        
        var user = [String:AnyObject]()
        user["user"] = params as AnyObject

        let headers: HTTPHeaders = [
            "token": token!,
        ]
        
        Alamofire.request(url, method: .put, parameters: user, headers: headers).validate().responseJSON { response in
            switch response.result {
                
            case .success(let json):
                
                let array = Mapper<UpdateProfile>().map(JSONObject:json) ?? nil
                //mapArray(JSONObject: json as AnyObject)!
                //let array = Mapper<UpdateProfile>().map(JSONObject: json as AnyObject)

                completion(array,nil)
                
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
}
