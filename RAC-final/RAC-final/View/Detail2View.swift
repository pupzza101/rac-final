//
//  Detail2View.swift
//  RAC-final
//
//  Created by MSU IT CS on 20/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import ParallaxHeader
import Floaty
class Detail2View: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var user_id:Int?
    var mapinfo:Getmap?
    var getfavinfo:GetFavpost?
    var favinfo = [CreateFavpost]()
    var deleteinfo:Deletepost?
    var image = [UIImage]()
    var imagepost = [UIImage]()
    var imgBase64 = [String]()
    var commentinfo = [GetComment]()
    var datecomment = [String]()
    var imageStr = [String]()
    func setHeaderView() -> Void {
        let headerview = HeaderDetailView.instanceFromNib()
        headerview.setUpView()
        headerview.setNeedsLayout()
        headerview.layoutIfNeeded()
        if getfavinfo == nil{
            if mapinfo?.status == "Wait"{
                headerview.statusLabel.text = "สถานะ : รอการช่วยเหลือ"
                
            } else if mapinfo?.status == "Helping"{
                headerview.statusLabel.text = "สถานะ : กำลังช่วยเหลือ"
                
            }else if mapinfo?.status == "Finish"{
                headerview.statusLabel.text = "สถานะ : ช่วยเหลือเสร็จสิ้น"
                
            }
            headerview.post_id = mapinfo?.id!
            if mapinfo?.nature != nil{
                headerview.helpcountLabel.text = "อาการ : \(mapinfo!.nature!)"
            }else{
                headerview.helpcountLabel.text = "อาการ : ไม่ระบุ"
            }
            
            if mapinfo?.images.count != 0{
                
                
                for tmp in (mapinfo?.images)!{
                    
                    
                    headerview.imageStr.append(tmp.image!)
                }
            }
        }else{
            if getfavinfo?.post?.status == "Wait"{
                headerview.statusLabel.text = "สถานะ : รอการช่วยเหลือ"
                
            } else if getfavinfo?.post?.status == "Helping"{
                headerview.statusLabel.text = "สถานะ : กำลังช่วยเหลือ"
                
            }else if getfavinfo?.post?.status == "Finish"{
                headerview.statusLabel.text = "สถานะ : ช่วยเหลือเสร็จสิ้น"
                
            }
            headerview.post_id = getfavinfo?.post?.post_id!
            
            if getfavinfo?.post?.nature != nil{
                headerview.helpcountLabel.text = "อาการ : \(getfavinfo!.post!.nature!)"
            }else{
                headerview.helpcountLabel.text = "อาการ : ไม่ระบุ"
            }
            if getfavinfo?.post?.images.count != 0{
                
                
                for tmp in (getfavinfo?.post?.images)!{
                    
                    
                    headerview.imageStr.append(tmp.image!)
                }
            }
        }
     
        
        tableview.parallaxHeader.view = headerview
        tableview.parallaxHeader.height = 300
        tableview.parallaxHeader.minimumHeight = 0
        tableview.parallaxHeader.mode = .bottomFill
        
        headerview.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.delegate = self
        tableview.dataSource = self
       tableview.separatorStyle = .none
        setHeaderView()
        self.navigationItem.title = "รายละเอียด"
        
        if getfavinfo == nil{
            if mapinfo?.images.count != 0{
                for tmp in (mapinfo?.images)!{
                    
                    let raw = "\(url!)/\(tmp.image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.imagepost.append(UIImage(data:data)!)
                    }
                    
                    
                }
                
                for tmp in imagepost{
                    
                    let imgData = tmp.jpegData(compressionQuality: 0.5)
                    self.imgBase64.append((imgData?.base64EncodedString())!)
                    
                    
                }
                
            }
        }else{
            if getfavinfo?.post?.images.count != 0{
                for tmp in (getfavinfo?.post?.images)!{
                    
                    let raw = "\(url!)/\(tmp.image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.imagepost.append(UIImage(data:data)!)
                    }
                    
                    
                }
                
                for tmp in imagepost{
                    
                    let imgData = tmp.jpegData(compressionQuality: 0.5)
                    self.imgBase64.append((imgData?.base64EncodedString())!)
                    
                    
                }
                
            }
        }
     
        let role = UserDefaults.standard.getrole()!
        let user_id = UserDefaults.standard.getuser_id()!
        let floaty = Floaty()
        
        floaty.addItem("เขียนคอมเม้นต์", icon: UIImage(named: "pencil"), handler: {
            item in
            self.performSegue(withIdentifier: "commentsegue", sender: self)

            floaty.close()
        })
        if getfavinfo == nil{
            if role == "2" || user_id == self.mapinfo?.creater?.user_id{
                floaty.addItem("ช่วยเหลือเสร็จสิ้น", icon: UIImage(named:"checkmark"), handler:{
                    item in
                    
                    let link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")
                    Updatepost.update(link!, token: token, title: self.mapinfo?.title!, description: self.mapinfo?.description!, status: "Finish", type_post: self.mapinfo?.type_post!, nature: self.mapinfo?.nature, image: self.imgBase64, titlelocation: self.mapinfo?.title!, latitude: self.mapinfo!.location?.latitude!, longtitude: self.mapinfo!.location?.longitude!){
                        (result,error) in
                        
                        if error == nil{
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        }
                    }
                    
                    floaty.close()
                    
                })
                floaty.addItem("เข้าช่วยเหลือ", icon: UIImage(named:"hospital"), handler:{
                    item in
                    let link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")
                    
                    Updatepost.update(link!, token: token, title: self.mapinfo?.title!, description: self.mapinfo?.description!, status: "Helping", type_post: self.mapinfo?.type_post!, nature: self.mapinfo?.nature, image: self.imgBase64, titlelocation: self.mapinfo?.title!, latitude: self.mapinfo!.location?.latitude!, longtitude: self.mapinfo!.location?.longitude!){
                        (result,error) in
                        
                        if error == nil{
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        }
                    }
                    
                    floaty.close()
                    
                })
            }
        }else{
            if role == "2" || user_id == self.getfavinfo?.post?.create?.user_id{
                
                floaty.addItem("ช่วยเหลือเสร็จสิ้น", icon: UIImage(named:"checkmark"), handler:{
                    item in
                    
                    let link = URL(string:"\(url!)/posts/\(self.getfavinfo!.post!.post_id!)")
                    Updatepost.update(link!, token: token, title: self.getfavinfo!.post!.location!.title!, description: self.getfavinfo!.post!.description!, status: "Finish", type_post: self.getfavinfo!.post!.type_post!, nature: self.getfavinfo!.post!.nature, image: self.imgBase64, titlelocation: self.getfavinfo!.post!.location!.title!, latitude: self.getfavinfo!.post!.location!.latitude!, longtitude: self.getfavinfo!.post!.location!.longitude!){
                        (result,error) in
                        
                        if error == nil{
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        }
                    }
                    
                    floaty.close()
                    
                })
                floaty.addItem("เข้าช่วยเหลือ", icon: UIImage(named:"hospital"), handler:{
                    item in
                    let link = URL(string:"\(url!)/posts/\(self.getfavinfo!.post!.post_id!)")

                  Updatepost.update(link!, token: token, title: self.getfavinfo!.post!.location!.title!, description: self.getfavinfo!.post!.description!, status: "Helping", type_post: self.getfavinfo!.post!.type_post!, nature: self.getfavinfo!.post!.nature, image: self.imgBase64, titlelocation: self.getfavinfo!.post!.location!.title!, latitude: self.getfavinfo!.post!.location!.latitude!, longtitude: self.getfavinfo!.post!.location!.longitude!){
                        (result,error) in
                        
                        if error == nil{
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        }
                    }
                    
                    floaty.close()
                    
                })
            }
        }
  
        
      
      
        floaty.paddingY = 30
        self.view.addSubview(floaty)
        
       
        
        self.user_id = UserDefaults.standard.getuser_id()
       // var idStr = String(describing: user_id!)
        if getfavinfo == nil{
            if user_id == mapinfo!.creater!.user_id! || role == "3"{
                
                let optionBtn = UIButton(type: .custom)
                // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
                optionBtn.addTarget(self, action: #selector(option), for: .touchUpInside)
                optionBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                optionBtn.setTitle("ตัวเลือก", for: .normal)
                optionBtn.setTitleColor(UIColor.blue, for: .normal)
                optionBtn.clipsToBounds = true
                
                let barButton = UIBarButtonItem(customView: optionBtn)
                
                self.navigationItem.rightBarButtonItem = barButton
            }
        }else{
            if user_id == getfavinfo!.post!.create!.user_id! || role == "3"{
                
                let optionBtn = UIButton(type: .custom)
                // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
                optionBtn.addTarget(self, action: #selector(option), for: .touchUpInside)
                optionBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                optionBtn.setTitle("ตัวเลือก", for: .normal)
                optionBtn.setTitleColor(UIColor.blue, for: .normal)
                optionBtn.clipsToBounds = true
                
                let barButton = UIBarButtonItem(customView: optionBtn)
                
                self.navigationItem.rightBarButtonItem = barButton
            }
        }
      
        let link:URL
        if getfavinfo == nil{
            
            link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)/comments")!
      
        }else{
            link = URL(string:"\(url!)/posts/\(self.getfavinfo!.post!.post_id!)/comments")!

        }

        GetComment.get(link, token: token){
            (result,error) in
            
            if result != nil{
                self.commentinfo = result!
                for tmp in self.commentinfo{
                    
                    if tmp.image != nil{
                        let raw = "\(url!)\(tmp.image!)"
                        let  Nsurl = NSURL(string: raw )!
                        
                        if let data = try? Data(contentsOf: Nsurl as URL)
                        {
                            //  images.append(UIImage(data: data)!)
                            self.image.append(UIImage(data:data)!)
                        }
                    }else{
                        self.image.append(UIImage(named: "noimage")!)
                    }
                  
                    
                    let myDateString =  tmp.updated_at!
                    let dateFormatter = DateFormatter()
                    
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    let date = dateFormatter.date(from: myDateString)
                    
                    dateFormatter.dateFormat = "dd.MM.yy:HH.mm"
                    self.datecomment.append(dateFormatter.string(from: date!))
                    
                }

            }
            
            self.tableview.reloadData()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.image.removeAll()
        self.datecomment.removeAll()
        let link:URL
        if getfavinfo == nil{
            link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)/comments")!
        }else{
            link = URL(string:"\(url!)/posts/\(self.getfavinfo!.post!.post_id!)/comments")!
            
        }
        GetComment.get(link, token: token){
            (result,error) in
            
            if result != nil{
                self.commentinfo = result!
                for tmp in self.commentinfo{
                    
                    if tmp.image != nil{
                        let raw = "\(url!)\(tmp.image!)"
                        let  Nsurl = NSURL(string: raw )!
                        
                        if let data = try? Data(contentsOf: Nsurl as URL)
                        {
                            //  images.append(UIImage(data: data)!)
                            self.image.append(UIImage(data:data)!)
                        }
                    }else{
                        self.image.append(UIImage(named: "noimage")!)
                    }
                    
                    let myDateString =  tmp.updated_at!
                    let dateFormatter = DateFormatter()
                    
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    let date = dateFormatter.date(from: myDateString)
                    
                    dateFormatter.dateFormat = "dd.MM.yy:HH.mm"
                    self.datecomment.append(dateFormatter.string(from: date!))
                    
                }
                
            }
            
            self.tableview.reloadData()
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "commentsegue"{
        if let next = segue.destination as? CommentView{
            if getfavinfo == nil{
                next.post_id = self.mapinfo?.id!

            }else{
                next.post_id = self.getfavinfo!.post!.post_id!

            }
        }
        }else {
            if let edit = segue.destination as? EditpostView{
                if getfavinfo == nil{
                    edit.mapinfo = self.mapinfo

                }else{
                    edit.favinfo = self.getfavinfo
                }
            }
        }
    }

    @objc func option(){
        let actionSheet = UIAlertController(title: "",message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "แก้ไขโพสต์", style: .default, handler: { (action:UIAlertAction) in
            
            self.performSegue(withIdentifier: "editsegue", sender: self)

            
            
        }))
        actionSheet.addAction(UIAlertAction(title: "ลบโพสต์", style: .default, handler: { (action:UIAlertAction) in
            UINavigationBar.appearance().tintColor = UIColor.black
            
            let alert = UIAlertController(title: "คุณต้องการจะลบโพสต์นี้หรือไม่ ?", message: nil, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: { action in
               // let link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")
                let link:URL
                if self.getfavinfo == nil{
                    link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")!
                }else{
                    link = URL(string:"\(url!)/posts/\(self.getfavinfo!.post!.post_id!)")!
                    
                }
                Deletepost.delete(link, token: token){
                    (result,error) in
                    
                    if result != nil{
                        
                        self.navigationController?.popToRootViewController(animated: true)

                    }
                    
                    
                }
                
                
            }))
          
           alert.addAction(UIAlertAction(title: "ยกเลิก", style: .cancel, handler: nil ))
            self.present(alert, animated: true)
        }))
        
          actionSheet.addAction(UIAlertAction(title: "ยกเลิก", style: .cancel, handler: nil ))
        self.present(actionSheet , animated: true ,completion: nil)
        
   
    }

}
extension Detail2View:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return commentinfo.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetaileCell
        cell.nameText.text = commentinfo[indexPath.row].name!
        cell.messageText.text = commentinfo[indexPath.row].description!
        cell.userimageView.image = image[indexPath.row]
        cell.jobText.text = ""
        cell.dateText.text = datecomment[indexPath.row]
        if commentinfo[indexPath.row].role_id == 1{
            cell.statusText.text = "ผู้ใช้ทั่วไป"
        }else if commentinfo[indexPath.row].role_id == 2{
            cell.statusText.text = "จิตอาสา"
        }else{
            cell.statusText.text = "admin"

        }
        return cell
    }
    
    
}
