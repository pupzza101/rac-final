//
//  SettingView.swift
//  RAC-final
//
//  Created by MSU IT CS on 22/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class SettingView: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var EditBt: UIButton!
    @IBOutlet weak var HelpBt: UIButton!
    @IBOutlet weak var HistoryBt: UIButton!
    @IBOutlet weak var ContactBt: UIButton!
    @IBOutlet weak var LogoutBt: UIButton!
    @IBOutlet weak var imageview: UIImageView!
   // @IBOutlet weak var changeBt: UIButton!
    
    var username:String?
    var token:String?
    var name:String?
    var email:String?
    var position:String?
    var tel:String?
    var image:String?
    var address:String?
    var gender:String?
    var status:String?
    var verify:String?
    var role:String?
    var info:GetUserinfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        self.view.backgroundColor = UIColor(red:0.96, green:0.36, blue:0.22, alpha:1.0)

        
        username = UserDefaults.standard.getusername()
        name = UserDefaults.standard.getname()
        email = UserDefaults.standard.getemail()
        position = UserDefaults.standard.getposition()
        tel = UserDefaults.standard.gettel()
        image = UserDefaults.standard.getimage()
        gender = UserDefaults.standard.getgender()
        address = UserDefaults.standard.getaddress()
        token = UserDefaults.standard.gettoken()
        verify = UserDefaults.standard.getverify()
        role = UserDefaults.standard.getrole()
        
        let raw = "\(url!)\(self.image!)"
        let  Nsurl = NSURL(string: raw )!
        
        if let data = try? Data(contentsOf: Nsurl as URL)
        {
            //  images.append(UIImage(data: data)!)
            imageview.image = UIImage(data:data)
        }
        if role! == "2"{
            statusLabel.text = "  ชื่อ \(name!)       (จิตอาสา)"
          //  changeBt.isHidden = true

        }else if role! == "3"
        {
            statusLabel.text = "  ชื่อ \(name!)       (ผู้ดูแล)"
           // changeBt.isHidden = true

            
        }else{
             statusLabel.text = "  ชื่อ \(name!)       (ผู้ใช้ทั่วไป)"
        }
        EditBt.addTarget(self, action: #selector(edit), for: .touchUpInside)
        HelpBt.addTarget(self, action: #selector(help), for: .touchUpInside)
        HistoryBt.addTarget(self, action: #selector(history), for: .touchUpInside)
        ContactBt.addTarget(self, action: #selector(contact), for: .touchUpInside)
        LogoutBt.addTarget(self, action: #selector(logout), for: .touchUpInside)
        
        imageview.layer.cornerRadius = imageview.frame.height/2
        imageview.clipsToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false

        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let user_id = UserDefaults.standard.getuser_id()
        let link = URL(string:"\(url!)/users/\(user_id!)")
        
        GetUserinfo.get(link!, token: token!){
            (result,error) in
            self.info = result
            UserDefaults.standard.saveinfo(username: self.info?.username, name: self.info?.name, email: self.info?.email, position: self.info?.position, tel: self.info?.phone, image: self.info?.image, role: self.info?.role, user_id: self.info?.user_id, gender: self.info?.gender, address: self.info?.address,verify:self.info?.verify)
            
            self.username = UserDefaults.standard.getusername()
            self.name = UserDefaults.standard.getname()
           self.email = UserDefaults.standard.getemail()
            self.position = UserDefaults.standard.getposition()
           self.tel = UserDefaults.standard.gettel()
           self.image = UserDefaults.standard.getimage()
            self.gender = UserDefaults.standard.getgender()
           self.address = UserDefaults.standard.getaddress()
            self.token = UserDefaults.standard.gettoken()
            self.verify = UserDefaults.standard.getverify()
            
            let raw = "\(url!)\(self.image!)"
            let  Nsurl = NSURL(string: raw )!
            
            if let data = try? Data(contentsOf: Nsurl as URL)
            {
                //  images.append(UIImage(data: data)!)
                self.imageview.image = UIImage(data:data)
            }
        }
    }
    @objc func edit(){
        
    }
    @objc func help(){
    }
    @objc func history(){
        performSegue(withIdentifier: "helpsegue", sender: self)

    }
    @objc func contact(){
        
    }
    @objc func logout(){
        let alert = UIAlertController(title: "คุณต้องการออกจากระบบหรือไม่ ?", message: nil, preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: { action in
            // let link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")
       
            UserDefaults.standard.logout()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "navivc")
            self.present(vc!, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "ยกเลิก", style: .cancel, handler: nil ))
        
        self.present(alert, animated: true)
    
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editsegue"{
            let next = segue.destination as! EditProfileView
           // next.topic = "แก้ไขข้อมูล"
           // next.username = self.username
            next.name = self.name
            next.email = self.email
            next.tel = self.tel
            next.address = self.address
            next.gender = self.gender
        //   next.mode = 2
            
           let raw = "\(url!)\(self.image!)"
            let  Nsurl = NSURL(string: raw )!
            
            if let data = try? Data(contentsOf: Nsurl as URL)
            {
                //  images.append(UIImage(data: data)!)
                next.image = UIImage(data:data)
            }
            
            
        }
    }
}
