//
//  GetmapView.swift
//  RAC-final
//
//  Created by MSU IT CS on 15/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class GetmapView: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {

    @IBOutlet weak var mapview: MKMapView!
    var mapLocationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    override func viewDidLoad() {
        super.viewDidLoad()

        mapLocationManager.delegate = self
        mapLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        mapview.delegate = self
        mapview.showsUserLocation = true
        mapLocationManager.startUpdatingLocation()
        let doneBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(save), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton

    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        mapLocationManager.delegate = self
        mapLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        mapview.delegate = self
        mapview.showsUserLocation = true
        mapLocationManager.startUpdatingLocation()
    }
    
    
    @objc func save(){
        print("save")
        print("latitude : \(latitude!)","longitude : \(longitude!)")
        UserDefaults.standard.savelocation(latitude: String(format:"%f", latitude!), longtitude: String(format:"%f", longitude!))
        self.navigationController?.popViewController(animated: true)


    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        GoToCenterLocation()
        let firstlocation = locations.first
        self.latitude = firstlocation?.coordinate.latitude
        self.longitude = firstlocation?.coordinate.longitude
        
    }
    func GoToCenterLocation() {
        if let locMan = mapLocationManager.location {
            let region = MKCoordinateRegion(center: locMan.coordinate, latitudinalMeters: 400, longitudinalMeters: 400)
            mapview.setRegion(region, animated: true)
        }
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? customPin else { return nil }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            let smallSquare = CGSize(width: 30, height: 30)
            let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
            button.setBackgroundImage(UIImage(named: "car"), for: .normal)
            
            view.calloutOffset = CGPoint(x: -5, y: 5)
            //view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            view.leftCalloutAccessoryView = button
            
        }
        return view
        
        
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                    calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! customPin
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}
