//
//  HelpView.swift
//  RAC-final
//
//  Created by MSU IT CS on 22/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
struct datatitle {
    var title:String?
}
class HelpView: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var titledata = [datatitle]()
    var titlesegue:String?
    var detailtypesegue:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ขั้นตอนการช่วยเหลือ"
        tableview.delegate = self
        tableview.dataSource = self
       
        titledata = [datatitle(title: "หมดสติ"),datatitle(title: "กระดูกหัก"),datatitle(title: "บาดเจ็บที่ศรีษะ")]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsegue" {
            let next = segue.destination as! HelpdetailView
            
            next.detailtype = self.detailtypesegue
            next.titlepost = self.titlesegue
        }
    }


}
extension HelpView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.titlesegue = titledata[indexPath.row].title
        self.detailtypesegue = indexPath.row
        
        performSegue(withIdentifier: "detailsegue", sender: self)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titledata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HelpCell
        
        cell.titleLabel.text = titledata[indexPath.row].title
        
        
        return cell
    }
    
    
    
}
