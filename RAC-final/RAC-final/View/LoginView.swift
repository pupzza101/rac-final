//
//  ViewController.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
var url = URL(string:"http://api.rac.arcsak.tk")
var token = UserDefaults.standard.gettoken()
class LoginView: UIViewController {

    
    @IBOutlet weak var logibBt: UIButton!
   
  //  @IBOutlet weak var FBlogin: FBSDKButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        logibBt.layer.cornerRadius = 5
        logibBt.layer.borderWidth = 0.5
        logibBt.clipsToBounds = true
        
        
     //   FBlogin.setTitle("เข้าสู่ระบบโดย Facebook", for: .normal)
        
        self.view.backgroundColor = UIColor(red:0.96, green:0.36, blue:0.22, alpha:1.0)
        
        
        
//        facebookBt.layer.cornerRadius = 5
//        facebookBt.layer.borderWidth = 0.5
//        facebookBt.layer.borderColor = UIColor.white.cgColor
//        facebookBt.clipsToBounds = true
    }
    
  /*  @IBAction func Facebooklogin(_ sender: Any) {
        let maneger = LoginManager()
       
        maneger.logIn(readPermissions: [.publicProfile,.email,.userGender], viewController: self) { (LoginResult) in
            switch LoginResult{
            case .cancelled:
                
                break
                
            case .failed(let error):
                
                break
            case .success(grantedPermissions: let grantedPermissions, declinedPermissions: let declinedPermissions, token: let accessToken):
                
                print("success")
                print(accessToken)
                self.getUserprofile()
            }
        }
    }*/
    func getUserprofile(){
        let connection = GraphRequestConnection()
        connection.add(GraphRequest(graphPath: "/me", parameters: ["fields": "name,email,gender"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: GraphAPIVersion.defaultVersion)){
                response,result in
            switch result {
            case .success(let response):
                
                print("Loggend in user facebook Name = \(response.dictionaryValue!["name"]!)")
                print("Loggend in user facebook email = \(response.dictionaryValue!["email"]!)")
                print("Loggend in user facebook email = \(response.dictionaryValue!["gender"]!)")

               // UserDefaults.standard.saveinfo(username: , name: response.dictionaryValue!["name"]!, email: response.dictionaryValue!["email"]!, position: self.info?.position, tel: self.info?.phone, image: , role: , user_id: , gender: , address: ,verify:self.info?.verify)
                
                
            case .failed(let error): print("We have error fetching loggendin user profile == \(error.localizedDescription)")
            }
        
        }
        connection.start()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    
    
    
}

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
