//
//  HeaderDetailView.swift
//  RAC-final
//
//  Created by MSU IT CS on 20/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class HeaderDetailView: UIView {

    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var checkboxView: UIButton!
    @IBOutlet weak var checkbox2View: UIButton!
    
    @IBOutlet weak var helpcountLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    var tmp:(() -> ())?
    var tmp2:(() -> ())?
    var imageStr = [String]()
    var images = [UIImage]()
    var post_id:Int?
    var fav_id:Int?
    var getfav = [GetFavpost]()
    class func instanceFromNib() -> HeaderDetailView {
        return UINib(nibName: "HeaderDetailView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HeaderDetailView
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let link = URL(string:"\(url!)/favoriteposts")
        GetFavpost.get(link!, token: token){
            (result,error) in
            if result != nil{
                
            
            self.getfav = result!
            
            
                
            for tmp in self.getfav{
                
                if tmp.post?.post_id == self.post_id{
                    self.checkboxView.isSelected = true
                    self.checkboxView?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                    self.fav_id = tmp.id
                }else{
                    self.checkboxView.isSelected = false
                    self.checkboxView?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
                }
                
                
            }
        }
        }
        
        checkboxView.layer.cornerRadius = checkboxView.frame.height/2
        checkboxView.clipsToBounds = true
        checkboxView.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        checkboxView.layer.borderWidth = 0.125
        
        
        checkbox2View.layer.cornerRadius = checkboxView.frame.height/2
        checkbox2View.clipsToBounds = true
        checkbox2View.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        checkbox2View.layer.borderWidth = 0.125
        
        collectionview.dataSource = self
        collectionview.delegate = self
        let nibcell = UINib(nibName: "ImageCollectionCell", bundle: nil)
        collectionview.register(nibcell, forCellWithReuseIdentifier: "ImageCollectionCell")
        
        if imageStr.count != 0{
            for tmp in imageStr{
                let raw = "\(url!)\(tmp)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    self.images.append(UIImage(data:data)!)
                }
            }
            images.removeFirst()
        }else{
            self.images = [UIImage(named:"noimage")!]
            

        }
       
     self.collectionview.reloadData()
    }
    func setUpView() -> Void {
        
        mainview.frame = self.bounds
        mainview.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    
    }
    
    @IBAction func checkbox1Btn(_ sender: Any) {
            
            let checkbox = checkboxView
        if ((checkbox!.isSelected)){
            checkbox!.isSelected = false
            checkbox?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
                
            print("don't check")
            let link = URL(string:"\(url!)/favoriteposts/\(self.fav_id!)")
            
            DeleteFavpost.delete(link!, token: token!){
                (result,error) in
              
            }
                
                
            }else{
            checkbox?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
                checkbox?.isSelected = true
                
            print("checked")

            let link = URL(string:"\(url!)/favoriteposts")
            CreateFavpost.create(link!, token: token!, post_id: self.post_id!){
                (result,error) in
           
            }
                
                
            }
            
        
    }
    @IBAction func checkbox2Btn(_ sender: Any) {
        
        let checkbox = checkbox2View
        if ((checkbox!.isSelected)){
            checkbox!.isSelected = false
            checkbox?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
            
            print("don't check")
            
            
            
        }else{
            checkbox?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
            checkbox?.isSelected = true
            
            print("checked")
            
            
            
            
        }
    }
    
    

}
extension HeaderDetailView:UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        
        cell.imageview.image = images[indexPath.row]
        
        
        return cell
        
    }
    
    
}
