//
//  CommentView.swift
//  RAC-final
//
//  Created by MSU IT CS on 18/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import ImagePicker
class CommentView: UIViewController {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var commentText: UITextView!
    var allimage = [UIImage]()
    var post_id:Int?
    let imagePickerController = ImagePickerController()
    var imgBase64:String?
    var message:CreateComment?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        // Do any additional setup after loading the view.
        commentText.delegate = self
        commentText.layer.borderWidth = 0.25
        commentText.text = "Comment Here"
        commentText.textColor = UIColor.lightGray
        let doneBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(createcomment), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    

    @IBAction func importimageBtn(_ sender: UIButton) {
        present(imagePickerController, animated: true, completion: nil)
        sender.setImage(UIImage(named: ""), for: .normal)

    }
    @objc func createcomment(){
        let link = URL(string:"\(url!)/comments")
        if allimage.count != 0{
            let imgData = allimage[0].jpegData(compressionQuality: 0.5)
            imgBase64 = imgData?.base64EncodedString()
        }
        
        CreateComment.create(link!, token: token, post_id: self.post_id, description: commentText.text!, image: imgBase64){
            (result,error) in
            self.message = result
            
            if self.message?.message == "Create comment successfully."{
                
                self.navigationController?.popViewController(animated: true)

            }
        }

    }

}
extension CommentView:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Comment Here"
            textView.textColor = UIColor.lightGray
        }
    }
    
}
extension CommentView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
        print("++")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        allimage.removeAll()
        allimage += images
        
        
        imagePicker.dismiss(animated: true){
            
            if self.allimage.count != 0{
                self.imageview.image = self.allimage[0]
                
            }
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
