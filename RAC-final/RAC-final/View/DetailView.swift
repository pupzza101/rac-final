//
//  DetailView.swift
//  RAC-final
//
//  Created by MSU IT CS on 17/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MapKit
class DetailView: UIViewController {

    var titlepost:String?
    var type:String?
    var name:String?
    var message:String?
    var userimage:UIImage?
    var postimage:UIImage?
    var mapinfo:Getmap?
    var favinfo:GetFavpost?
    var streetST:String?
    var SubLocalityST:String?
    var SubAdministrativeAreaST:String?
    var cityST:String?
    var zipST:String?
    var countryST:String?
    @IBOutlet weak var nametext: UILabel!
    @IBOutlet weak var titletext: UILabel!
    @IBOutlet weak var typetext: UILabel!
    @IBOutlet weak var messagetext: UILabel!
    @IBOutlet weak var locationBt: UIButton!
    @IBOutlet weak var userimageview: UIImageView!
    @IBOutlet weak var postimageview: UIImageView!
    
    func converst(){
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        
        
        if self.favinfo == nil{
            titletext.text = "หัวข้อ : \(mapinfo!.title!)"
            
            
            if self.mapinfo!.type_post == "Accident"{
                typetext.text = "หมวดหมู่ : อุบัติเหตุ"
                
            }else{
                typetext.text = "หมวดหมู่ : อาชญากรรม"
            }
            messagetext.text = mapinfo?.description!
            nametext.text = mapinfo?.creater?.name!
            if mapinfo?.creater?.image != nil && mapinfo?.creater?.image != "" {
                let raw = "\(url!)\(mapinfo!.creater!.image!)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    userimageview.image = UIImage(data:data)!
                }
            }else{
                userimageview.image = UIImage(named:"user1")
            }
            
            if mapinfo!.images.count != 0{
                let raw = "\(url!)\(mapinfo!.images[0].image!)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    postimageview.image = UIImage(data:data)!
                }
                
            }else{
                
                postimageview.image = UIImage(named: "noimage")!
            }
            self.navigationItem.title = mapinfo?.title!
        }else{
            titletext.text = "หัวข้อ : \(favinfo!.post!.title!)"
            
            
            if self.favinfo?.post?.type_post! == "Accident"{
                typetext.text = "หมวดหมู่ : อุบัติเหตุ"
                
            }else{
                typetext.text = "หมวดหมู่ : อาชญากรรม"
            }
            messagetext.text = favinfo?.post?.description!
            nametext.text = favinfo?.post?.create?.name!
            if favinfo?.post?.create!.image! != nil && favinfo?.post?.create!.image! != "" {
                let raw = "\(url!)\(favinfo!.post!.create!.image!)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    userimageview.image = UIImage(data:data)!
                }
            }else{
                userimageview.image = UIImage(named:"user1")
            }
            
            if favinfo?.post?.images.count != 0{
                
                let raw = "\(url!)\(favinfo!.post!.images[0].image!)"
                let  Nsurl = NSURL(string: raw )!
                
                if let data = try? Data(contentsOf: Nsurl as URL)
                {
                    //  images.append(UIImage(data: data)!)
                    postimageview.image = UIImage(data:data)!
                }
                
            }else{
                
                postimageview.image = UIImage(named: "noimage")!
            }
            self.navigationItem.title = favinfo?.post?.title!
        }
      
        
        corner(nametext)
        corner(titletext)
        corner(typetext)
        corner(messagetext)

        let detailBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        detailBtn.addTarget(self, action: #selector(detail), for: .touchUpInside)
        detailBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        detailBtn.setTitle("รายละเอียด", for: .normal)
        detailBtn.setTitleColor(UIColor.blue, for: .normal)
        detailBtn.clipsToBounds = true
        
        locationBt.addTarget(self, action: #selector(openmap), for: .touchUpInside)
        
        let barButton = UIBarButtonItem(customView: detailBtn)
        
        self.navigationItem.rightBarButtonItem = barButton
       
        userimageview.layer.cornerRadius = userimageview.frame.height/2
        userimageview.clipsToBounds = true
        
        let geoCoder = CLGeocoder()
        var location = CLLocation()
        if favinfo == nil{
             location = CLLocation(latitude:  Double(self.mapinfo!.location!.latitude!)!, longitude: Double(self.mapinfo!.location!.longitude!)!)
        }else{
             location = CLLocation(latitude:  Double(self.favinfo!.post!.location!.latitude!)!, longitude: Double(self.favinfo!.post!.location!.longitude!)!)
        }
        
        
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
        
            // Address dictionary
           // print(placeMark.addressDictionary)

            // Location name
//            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
//                print(locationName)
//            }

            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
                 self.streetST = street as? String
                
            }
            if let SubLocality = placeMark.addressDictionary!["SubLocality"] as? NSString {
                print(SubLocality)
                self.SubLocalityST = SubLocality as? String
            }
            if let SubAdministrativeArea = placeMark.addressDictionary!["SubAdministrativeArea"] as? NSString {
                print(SubAdministrativeArea)
                 self.SubAdministrativeAreaST = SubAdministrativeArea as? String
            }
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
                self.cityST = city as? String

            }

            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
                self.zipST = zip as? String

            }

            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                print(country)
                self.countryST = country as? String

            }
               self.locationBt.setTitle(" \(self.SubLocalityST ?? "") \(self.SubAdministrativeAreaST ?? "") \(self.cityST ?? "") \(self.zipST ?? "") \(self.countryST ?? "")", for: .normal)
        })
        
     
    }
    
    
    func corner(_ label:UILabel){
        
        label.clipsToBounds = true
        label.layer.cornerRadius = 5
        label.layer.borderWidth = 0.25
        
    }
    
    @objc func openmap(){
       openMapForPlace()
        
    }
    func openMapForPlace() {
        
        var  lat1 : NSString
        var lng1 : NSString
        if favinfo == nil{
            lat1 = self.mapinfo?.location?.latitude! as! NSString
            lng1 = self.mapinfo?.location?.longitude! as! NSString
        }else{
            lat1 = self.favinfo!.post!.location!.latitude! as! NSString
            lng1 = self.favinfo!.post!.location!.longitude! as! NSString
        }
       
        
        let latitude:CLLocationDegrees =  lat1.doubleValue
        let longitude:CLLocationDegrees =  lng1.doubleValue
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        if favinfo == nil{
            mapItem.name = "\(self.mapinfo!.title!)"
        }else{
            mapItem.name = "\(self.favinfo!.post!.location!.title!)"

        }
        mapItem.openInMaps(launchOptions: options)
        
    }
    @objc func detail(){
        
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsegue" {
            let next = segue.destination as! Detail2View
            
            if favinfo == nil{
                 next.mapinfo = self.mapinfo
            }else{
                next.getfavinfo = self.favinfo
            }
           
        }
    }
}
