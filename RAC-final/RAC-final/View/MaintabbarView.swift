//
//  MaintabbarView.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class MaintabbarView: UITabBarController, UITabBarControllerDelegate{
    
    var str:String?
    var mode:Int = 1
    var viewmode:Int = 2
    override func viewDidLoad() {
        self.delegate = self
        super.viewDidLoad()
        if mode != 1 {
            self.selectedIndex = 1
        }
        guard let viewControllers = viewControllers else {
            return
        }
        
        for viewController in viewControllers{
            if let firstpageNavigationView = viewController as? FirstpageNavigationView{
                
                if let firstpageView = firstpageNavigationView.viewControllers.first as? Firstpage{
                    firstpageView.str = self.str
                   
                }
                
                
            }else if let settingNavigationView = viewController as? SetiingNavigationView{
                
                if let settingView = settingNavigationView.viewControllers.first as? SettingView{
                    settingView.status = self.str
                    //firstpageView.str = self.str
                }
            }
        }
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
        if tabBarController.selectedIndex == 0 {
            self.viewmode = 2
            
        }
    }
}
