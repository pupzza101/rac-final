//
//  NotifyView.swift
//  RAC-final
//
//  Created by MSU IT CS on 17/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DropDown
import DLRadioButton
import ImagePicker
class NotifyView: UIViewController {

    
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var typedropdownBt: UIButton!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var invimodeBt: UIButton!
    @IBOutlet weak var rdl1: DLRadioButton!
    
    @IBOutlet weak var rdl2: DLRadioButton!
    var token:String?
    var lati:String?
    var longgi:String?
    let dropdown = DropDown()
    var allimage = [UIImage]()
    var mode:Int?
    var latitude:String?
    var longitude:String?
    var type:String?
    var subtype:String? = ""
    var invimode:Bool = true
    
      let imagePickerController = ImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        invimodeBt.layer.cornerRadius = invimodeBt.frame.height/2
        invimodeBt.clipsToBounds = true
        invimodeBt.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        invimodeBt.layer.borderWidth = 0.125
        
        titleText.text = ""
        token = UserDefaults.standard.gettoken()
        usernameText.text = "username : \(UserDefaults.standard.getusername()!)"
        nameText.text = UserDefaults.standard.getname()
        let raw = "\(url!)\(UserDefaults.standard.getimage()!)"
        let  Nsurl = NSURL(string: raw )!
        
        if let data = try? Data(contentsOf: Nsurl as URL)
        {
            //  images.append(UIImage(data: data)!)
            imageview.image = UIImage(data:data)
        }
        imageview.layer.cornerRadius = imageview.frame.height/2
        imageview.clipsToBounds = true
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 5
       
        
        let doneBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(createpost), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton

    }
  

    @IBAction func inviBtn(_ sender: Any) {
        let invi = invimodeBt
        if ((invi!.isSelected)){
            invi!.isSelected = false
            invi?.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
            
          
            invimode = true
            
        }else{
            invi?.backgroundColor = UIColor(red:0.11, green:0.62, blue:0.76, alpha:1.0)
            invi?.isSelected = true
            
            invimode = false
            print("checked")
            
            
            
        }
    }
    
    @IBAction func locationBtn(_ sender: Any) {
        mode = 2
      

    }
    
    @IBAction func importimageBtn(_ sender: Any) {
        present(imagePickerController, animated: true, completion: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        titleText.text = nil
        lati = nil
        longgi = nil
        descriptionText.text = nil
        allimage.removeAll()
        invimodeBt.isSelected = false
        invimodeBt.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        invimode = true
        rdl1.isSelected = false
        rdl2.isSelected = false
        type = nil
        subtype = ""
        typedropdownBt.isHidden = true
    }
  
     @objc func createpost(){
        print("create")
        let link = URL(string:"\(url!)/posts")
        var imageBase64 = [String]()
        for i in allimage{
            let imgData = i.jpegData(compressionQuality: 0.5)
            imageBase64.append((imgData?.base64EncodedString())!)
           // imageBase64.append(convertImageTobase64(format: .jpeg(1.0), image: i)!)
        }
        if lati == nil {
        
            let alert = UIAlertController(title: "กรุณาระบุตำแหน่ง", message: nil, preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "ตกลง", style: .cancel, handler: nil ))
            self.present(alert, animated: true)
        }else if titleText.text! == "" || descriptionText.text! == "" || type == nil || subtype! == ""{
            let alert = UIAlertController(title: "กรุณากรอกข้อมูลให้ครบถ้วน", message: "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: {
                action in
                
                
            }))
            
            
            self.present(alert, animated: true)
        }
        else{
            Createpost.create(link!,token:token!, title: titleText.text!, description: descriptionText.text!, type_post: type!, nature: self.subtype!, identify: invimode, image: imageBase64, titlelocation: titleText.text!, latitude: latitude!, longtitude: longitude!){
                (result,error) in
                
                if error != nil{
                    print(error)
                }else{
                    let alert = UIAlertController(title: "โพสต์สำเร็จ", message: nil, preferredStyle: .alert)
                    
                    
                    alert.addAction(UIAlertAction(title: "ตกลง", style: .cancel, handler: nil ))
                    self.present(alert, animated: true)
                }
            }
        }
    
    }
    override func viewWillAppear(_ animated: Bool) {
        latitude = UserDefaults.standard.getlatitude()
        longitude = UserDefaults.standard.getlongtitude()
        
        lati = latitude
        longgi = longitude
        
    }
  
    @IBAction func typeBt(_ sender: DLRadioButton) {
        
        if sender.tag == 1{
            print("1")
            type = "Accident"
            typedropdownBt.isHidden = false
            dropdown.anchorView = typedropdownBt
            dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
            dropdown.dataSource = [ "หมดสติ", "อาการชัก","บาดเจ็บที่ศีรษะ","กระดูกหัก"]
            
            typedropdownBt.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
            dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.typedropdownBt.setTitle(item, for: .normal)
                
                self.subtype = item
            }
            
            typedropdownBt.layer.cornerRadius = 10
            typedropdownBt.clipsToBounds = true
            typedropdownBt.layer.borderWidth = 0.125
            
            
            
        }else{
            print("2")
            typedropdownBt.isHidden = true
             type = "Crime"
        }
    }
    
    
    @objc func dropdownAction(){
        dropdown.show()
        
    }


}
extension NotifyView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
        print("++")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        allimage.removeAll()
        allimage += images
        
        
        imagePicker.dismiss(animated: true){
            
//            if self.allimage.count != 0{
//                self.imageview.image = self.allimage[0]
//                
//            }
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
