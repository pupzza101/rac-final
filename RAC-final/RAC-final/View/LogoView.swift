//
//  LogoView.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class LogoView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { // Change `2.0` to the desired number of seconds.
            // Code you want to be delayed
            if UserDefaults.standard.gettoken() != nil{
                
                self.performSegue(withIdentifier: "FormView", sender: self)
                
            }
            else{ self.performSegue(withIdentifier: "loginView", sender: self)
                
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
