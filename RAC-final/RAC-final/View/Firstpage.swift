//
//  Firstpage.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

struct data {
    var title:String?
    var name:String?
    var type:String?
    var date:String?
    var image:UIImage?
    var status:UIImage?
}

class Firstpage: UIViewController,NVActivityIndicatorViewable {

    var offset:Int = 0
    var viewmode:Int = 2
    var str:String?
    var datasource = [data]()
    var titlesegue:String?
    var mapinfo = [Getmap]()
    var mapinfoload = [Getmap]()

    var image = [UIImage]()
    var datepost = [String]()
    var mapinfosegue:Getmap?
    var favinfo = [GetFavpost]()
    var favinfosegue:GetFavpost?
    var isloadform:Bool? = true
    private let refreshControl = UIRefreshControl()

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableview.separatorStyle = .none
        
        datasource = [data(title: "รถชน", name: "Rutcha", type: "accident",date:"10/10/10", image: nil
            , status:  nil)]
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        self.navigationItem.title = "หน้าแรก"
        
      /*  let alarmBtn = UIButton(type: .custom)
         alarmBtn.setImage(UIImage(named: "alarm"), for: .normal)
        alarmBtn.addTarget(self, action: #selector(alarmmode), for: .touchUpInside)
        alarmBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        alarmBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: alarmBtn)
        
        self.navigationItem.leftBarButtonItem = barButton*/

        let savepost = UIButton(type: .custom)
        savepost.setImage(UIImage(named: "text-line-form"), for: .normal)
        savepost.addTarget(self, action: #selector(savepostmode), for: .touchUpInside)
        savepost.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        savepost.clipsToBounds = true
        
        let barButton1 = UIBarButtonItem(customView: savepost)
        
        self.navigationItem.rightBarButtonItem = barButton1
        
    
      
       self.tableview.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)

    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.global(qos: .background).async {
          self.startAnimating()
            self.viewmode = 2
            let link = URL(string:"\(url!)/posts")
            self.offset = 0
            self.isloadform = true
            Getmap.check(link!, token: token!,offset: self.offset){
                (result,error) in
                if result != nil{
                    self.mapinfo = result!
                    for tmp in self.mapinfo{
                        
                        if tmp.images.count != 0{
                            let raw = "\(url!)\(tmp.images[0].image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.image.append(UIImage(data:data)!)
                            }
                            
                        }else{
                            self.image.append(UIImage(named: "noimage")!)
                        }
                        
                        //2019-06-13T09:21:25.168Z
                        //yyyy-MM-dd'T'HH:mm:ssZ
                        
                        
                        let myDateString =  tmp.updated_at!
                        let dateFormatter = DateFormatter()
                        
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let date = dateFormatter.date(from: myDateString)
                        
                        dateFormatter.dateFormat = "dd.MM.yy"
                        self.datepost.append(dateFormatter.string(from: date!))
                        
                     
                    }
                    DispatchQueue.main.async {
                        // reload your collection view here:
                        
                        self.tableview.reloadData()
                        print("mapinfo : \(self.mapinfo.count)")
                        self.stopAnimating()
                        
                    }
                }
                
            }
            
            
            
          
        }
     
    }
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        fetchWeatherData()
    }
    private func fetchWeatherData() {
        offset = 0
        isloadform = true

        self.image.removeAll()
        self.datepost.removeAll()
        self.viewmode = 2
        self.navigationItem.title = "หน้าแรก"

        let link = URL(string:"\(url!)/posts")
        Getmap.check(link!, token: token!, offset: offset){
            (result,error) in
            DispatchQueue.main.async {
            self.mapinfo = result!
            for tmp in self.mapinfo{
                
                
                if tmp.images.count != 0{
                    let raw = "\(url!)\(tmp.images[0].image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.image.append(UIImage(data:data)!)
                    }
                    
                }else if tmp.images.count == 0{
                    self.image.append(UIImage(named: "noimage")!)
                }
                //2019-06-13T09:21:25.168Z
                //yyyy-MM-dd'T'HH:mm:ssZ
                
                
                let myDateString =  tmp.updated_at!
                let dateFormatter = DateFormatter()
                
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                let date = dateFormatter.date(from: myDateString)
                
                dateFormatter.dateFormat = "dd.MM.yy"
                self.datepost.append(dateFormatter.string(from: date!))
                
            }
                
                self.refreshControl.endRefreshing()
                self.tableview.reloadData()

         }
        }
    }
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsegue" {
            let next = segue.destination as! DetailView
           
            next.titlepost = self.titlesegue
            if viewmode == 1{
                next.favinfo = self.favinfosegue
            }else{
                 next.mapinfo = self.mapinfosegue
            }
           
        }
    }
    
    
    @objc func alarmmode(){
        viewmode = 0
        self.navigationItem.title = "แจ้งเตือน"

        tableview.reloadData()
    }
    @objc func savepostmode(){
        if viewmode != 1 {
            viewmode = 1
            self.navigationItem.title = "โพสต์ที่บันทึก"
            
            let link = URL(string:"\(url!)/favoriteposts")
            
            GetFavpost.get(link!, token: token){
                (result,error) in
                if result != nil{
                    self.favinfo = result!
                    self.image.removeAll()
                    self.datepost.removeAll()
                    for tmp in self.favinfo{
                        
                        if tmp.post!.images.count != 0{
                            let raw = "\(url!)\(tmp.post!.images[0].image!)"
                            let  Nsurl = NSURL(string: raw )!
                            
                            if let data = try? Data(contentsOf: Nsurl as URL)
                            {
                                //  images.append(UIImage(data: data)!)
                                self.image.append(UIImage(data:data)!)
                            }
                            
                        }else{
                            self.image.append(UIImage(named: "noimage")!)
                        }
                        
                        //2019-06-13T09:21:25.168Z
                        //yyyy-MM-dd'T'HH:mm:ssZ
                        
                        
                        let myDateString =  tmp.updated_at!
                        let dateFormatter = DateFormatter()
                        
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        let date = dateFormatter.date(from: myDateString)
                        
                        dateFormatter.dateFormat = "dd.MM.yy"
                        self.datepost.append(dateFormatter.string(from: date!))
                        
                    }
                }
                
                self.tableview.reloadData()
            }
            
        }else{
            
            viewmode = 2
            self.navigationItem.title = "หน้าแรก"
            DispatchQueue.global(qos: .background).async {
                self.startAnimating()
                let link = URL(string:"\(url!)/posts")
                self.offset = 0
                self.isloadform = true
                Getmap.check(link!, token: token!,offset: self.offset){
                    (result,error) in
                    if result != nil{
                        self.mapinfo = result!
                        for tmp in self.mapinfo{
                            
                            if tmp.images.count != 0{
                                let raw = "\(url!)\(tmp.images[0].image!)"
                                let  Nsurl = NSURL(string: raw )!
                                
                                if let data = try? Data(contentsOf: Nsurl as URL)
                                {
                                    //  images.append(UIImage(data: data)!)
                                    self.image.append(UIImage(data:data)!)
                                }
                                
                            }else{
                                self.image.append(UIImage(named: "noimage")!)
                            }
                            
                            //2019-06-13T09:21:25.168Z
                            //yyyy-MM-dd'T'HH:mm:ssZ
                            
                            
                            let myDateString =  tmp.updated_at!
                            let dateFormatter = DateFormatter()
                            
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            let date = dateFormatter.date(from: myDateString)
                            
                            dateFormatter.dateFormat = "dd.MM.yy"
                            self.datepost.append(dateFormatter.string(from: date!))
                            
                            
                        }
                        DispatchQueue.main.async {
                            // reload your collection view here:
                            
                            self.tableview.reloadData()
                            print("mapinfo : \(self.mapinfo.count)")
                            self.stopAnimating()
                            
                        }
                    }
                    
                }
                
                
                
                
            }
            

        }
      
    }

}
extension Firstpage:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        if viewmode == 0{
            
        }else if viewmode == 1{
           if indexPath.row == self.favinfo.count - 1
           {
            
            
            }
        }else
        {
           if indexPath.row == self.mapinfo.count - 1
           {
            if isloadform == true{
                self.offset = self.offset + 5
                 print("++")
                let link = URL(string:"\(url!)/posts")
                Getmap.check(link!, token: token!,offset: offset){
                    (result,error) in
                    
                    if result != nil{
                        self.mapinfoload = result!
                    }else{
                        self.isloadform = false
                    }
                    
                    if self.mapinfoload.count < 5{
                        self.isloadform = false
                        print(self.offset)
                        
                    }
                    if self.mapinfoload.count != 0{
                        
                        for tmp in self.mapinfoload{
                            
                            self.mapinfo.append(tmp)
                            if tmp.images.count != 0{
                                let raw = "\(url!)\(tmp.images[0].image!)"
                                let  Nsurl = NSURL(string: raw )!
                                
                                if let data = try? Data(contentsOf: Nsurl as URL)
                                {
                                    //  images.append(UIImage(data: data)!)
                                    self.image.append(UIImage(data:data)!)
                                }
                                
                            }else if tmp.images.count == 0{
                                self.image.append(UIImage(named: "noimage")!)
                            }
                            
                            
                            let myDateString =  tmp.updated_at!
                            let dateFormatter = DateFormatter()
                            
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            let date = dateFormatter.date(from: myDateString)
                            
                            dateFormatter.dateFormat = "dd.MM.yy"
                            self.datepost.append(dateFormatter.string(from: date!))
                            
                        }
                       
                        self.tableview.reloadData()
                    }
                    
                    
                    
                
                    
                }

             }
           }
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
    //    self.titlesegue = datasource[indexPath.row].title
        if viewmode == 1{
            self.favinfosegue = favinfo[indexPath.row]
        }else{
            self.mapinfosegue = mapinfo[indexPath.row]
        }
        
//        idanswer = user_form[indexPath.row].id
//        namesegue = user_form[indexPath.row].name
//        datesegue = somedateString[indexPath.row]
//        let raw = "\(url!)\(user_form[indexPath.row].image!)"
//        let  Nsurl = NSURL(string: raw )!
//
//        if let data = try? Data(contentsOf: Nsurl as URL)
//        {
//            //  images.append(UIImage(data: data)!)
//            imagesegue = UIImage(data:data)
//        }
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if viewmode == 0{
            
        }else if viewmode == 1{
            return favinfo.count
        }else
        {
            return mapinfo.count

        }
        return Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FirstpageCell
        
        if viewmode == 0{
            
        }else if viewmode == 1{
            
            cell.nameLabel.text = favinfo[indexPath.row].post?.create?.name!
            cell.title.text = favinfo[indexPath.row].post?.title!
            // cell.nameLabel.text = mapinfo[indexPath.row].name!
            if self.favinfo[indexPath.row].post?.type_post! == "Accident"{
                cell.typeLabel.text = "| อุบัติเหตุ"
                
            }else{
                cell.typeLabel.text = "| อาชญากรรม"
            }
            if image.count != 0{
                cell.imageview.image = image[indexPath.row]
                
            }
            if datepost.count != 0{
                cell.dateLabel.text = datepost[indexPath.row]
                
            }
            
            if favinfo[indexPath.row].post?.status! == "Finish"{
                cell.statusimageview.image = UIImage(named: "checkmark")
            }else if mapinfo[indexPath.row].status! == "Helping"{
                cell.statusimageview.image = UIImage(named: "stopwatch")
                
            }else{
                cell.statusimageview.image = UIImage(named: "error")
                
            }
            return cell
        }else{
            cell.nameLabel.text = mapinfo[indexPath.row].creater?.name!
            cell.title.text = mapinfo[indexPath.row].title!
           // cell.nameLabel.text = mapinfo[indexPath.row].name!
            if self.mapinfo[indexPath.row].type_post == "Accident"{
                cell.typeLabel.text = "| อุบัติเหตุ"

            }else{
                cell.typeLabel.text = "| อาชญากรรม"
            }
            if image.count != 0{
                cell.imageview.image = image[indexPath.row]

            }
            if datepost.count != 0{
                cell.dateLabel.text = datepost[indexPath.row]

            }
            
            if mapinfo[indexPath.row].status! == "Finish"{
                cell.statusimageview.image = UIImage(named: "checkmark")
            }else if mapinfo[indexPath.row].status! == "Helping"{
                cell.statusimageview.image = UIImage(named: "stopwatch")

            }else{
                cell.statusimageview.image = UIImage(named: "error")

            }
             return cell
        }
        return UITableViewCell()
    }
    
    
    
    
}
