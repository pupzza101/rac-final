//
//  VerifyView.swift
//  RAC-final
//
//  Created by MSU IT CS on 25/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import ImagePicker

class VerifyView: UIViewController {

    @IBOutlet weak var imageview1: UIImageView!
    @IBOutlet weak var imageview2: UIImageView!
    var allimage = [UIImage]()
    var allimage2 = [UIImage]()
    let imagePickerController1 = ImagePickerController()
    let imagePickerController2 = ImagePickerController()
    var mode:Int?
    var imgBase64:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController1.delegate = self
        imagePickerController2.delegate = self
        imagePickerController1.imageLimit = 1
        imagePickerController2.imageLimit = 1

        let doneBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(verify), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func verify(){
        let link = URL(string:"\(url!)/users")
        if allimage2.count != 0{
            let imgData = allimage2[0].jpegData(compressionQuality: 0.5)
            imgBase64 = imgData?.base64EncodedString()
        }
        UpdateProfile.update(link!, user_id: UserDefaults.standard.getuser_id()!, name: UserDefaults.standard.getname()!, email: UserDefaults.standard.getemail()!, tel: UserDefaults.standard.gettel()!, image: nil, gender: UserDefaults.standard.getgender()!, address: UserDefaults.standard.getaddress()!, verify: 1, verify_image: imgBase64){
            (result,error) in
            
            if error == nil{
                self.navigationController?.popViewController(animated: true)
            }else{
                print(error)
            }
        }
    }
    
    @IBAction func importimage1Btn(_ sender: Any) {
        mode = 1
        present(imagePickerController1, animated: true, completion: nil)

    }
    
    @IBAction func importimage2(_ sender: Any) {
        mode = 2
        present(imagePickerController2, animated: true, completion: nil)

    }
    
}
extension VerifyView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
        print("++")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
       
        if mode == 1{
            allimage.removeAll()
            allimage += images
        }else{
             allimage2.removeAll()
            allimage2 += images
        }
        
        
        
        imagePicker.dismiss(animated: true){
            
            if self.allimage.count != 0 {
                
                    self.imageview1.image = self.allimage[0]
            }
            if self.allimage2.count != 0{
                self.imageview2.image = self.allimage2[0]
            }
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
