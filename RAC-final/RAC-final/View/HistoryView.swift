//
//  HistoryView.swift
//  RAC-final
//
//  Created by MSU IT CS on 11/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class HistoryView: UIViewController {

  
    var viewmode:Int = 2
    var str:String?
    var datasource = [data]()
    var titlesegue:String?
    var mapinfo = [Getmap]()
    var image = [UIImage]()
    var datepost = [String]()
    var mapinfosegue:Getmap?
    var favinfo = [GetFavpost]()
    var favinfosegue:GetFavpost?
    var offset:Int = 0
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ประวัติการโพสต์"
        datasource = [data(title: "รถชน", name: "Rutcha", type: "accident",date:"10/10/10", image: nil
            , status:  nil)]
        tableview.delegate = self
        tableview.dataSource = self
        
        let link = URL(string:"\(url!)/myposts")

        Getmap.check(link!, token: token!, offset: nil){
            (result,error) in
            
            if result != nil{
                self.mapinfo = result!
                for tmp in self.mapinfo{
                    
                    if tmp.images.count != 0{
                        let raw = "\(url!)\(tmp.images[0].image!)"
                        let  Nsurl = NSURL(string: raw )!
                        
                        if let data = try? Data(contentsOf: Nsurl as URL)
                        {
                            //  images.append(UIImage(data: data)!)
                            self.image.append(UIImage(data:data)!)
                        }
                        
                    }else{
                        self.image.append(UIImage(named: "noimage")!)
                    }
                    
                    //2019-06-13T09:21:25.168Z
                    //yyyy-MM-dd'T'HH:mm:ssZ
                    
                    
                    let myDateString =  tmp.updated_at!
                    let dateFormatter = DateFormatter()
                    
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    let date = dateFormatter.date(from: myDateString)
                    
                    dateFormatter.dateFormat = "dd.MM.yy"
                    self.datepost.append(dateFormatter.string(from: date!))
                    
                }
                self.tableview.reloadData()
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsegue" {
            let next = segue.destination as! DetailView
            
            next.titlepost = self.titlesegue
            next.mapinfo = self.mapinfosegue

        }
    }

}

extension HistoryView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
        self.titlesegue = datasource[indexPath.row].title
        self.mapinfosegue = mapinfo[indexPath.row]

        //        idanswer = user_form[indexPath.row].id
        //        namesegue = user_form[indexPath.row].name
        //        datesegue = somedateString[indexPath.row]
        //        let raw = "\(url!)\(user_form[indexPath.row].image!)"
        //        let  Nsurl = NSURL(string: raw )!
        //
        //        if let data = try? Data(contentsOf: Nsurl as URL)
        //        {
        //            //  images.append(UIImage(data: data)!)
        //            imagesegue = UIImage(data:data)
        //        }
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mapinfo.count


    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryCell

        cell.nameLabel.text = mapinfo[indexPath.row].creater?.name!
        cell.title.text = mapinfo[indexPath.row].title!
        // cell.nameLabel.text = mapinfo[indexPath.row].name!
        if self.mapinfo[indexPath.row].type_post == "Accident"{
            cell.typeLabel.text = "| อุบัติเหตุ"
            
        }else{
            cell.typeLabel.text = "| อาชญากรรม"
        }
        if image.count != 0{
            cell.imageview.image = image[indexPath.row]
            
        }
        if datepost.count != 0{
            cell.dateLabel.text = datepost[indexPath.row]
            
        }
        
        if mapinfo[indexPath.row].status! == "Finish"{
            cell.statusimageview.image = UIImage(named: "checkmark")
        }else if mapinfo[indexPath.row].status! == "Helping"{
            cell.statusimageview.image = UIImage(named: "stopwatch")
            
        }else{
            cell.statusimageview.image = UIImage(named: "error")
            
        }
        return cell

    }
    
    
    
    
}
