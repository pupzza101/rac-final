//
//  MapView.swift
//  RAC-final
//
//  Created by MSU IT CS on 20/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class customPin: NSObject,MKAnnotation{
    
    var coordinate:CLLocationCoordinate2D
    var title:String?
    var subtitle: String?
    
    init(pinTitle:String,pinSubTitle:String,location:CLLocationCoordinate2D) {
        
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
        
    }
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}


class MapView: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {

    @IBOutlet weak var navigation: UINavigationBar!
    @IBOutlet weak var saveBt: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    var mapLocationManager = CLLocationManager()
    var latitude:Double?
    var longitude:Double?
    var mode:Int = 1
    var selectedPin:MKPlacemark? = nil
    var token:String?
    var maplocation = [Getmap]()
    override func viewDidLoad() {
        super.viewDidLoad()
        token = UserDefaults.standard.gettoken()
        mapLocationManager.delegate = self
        mapLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse && CLLocationManager.authorizationStatus() != . authorizedAlways {
            mapLocationManager.requestWhenInUseAuthorization()
        }
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapLocationManager.startUpdatingLocation()
        
        let link = URL(string:"\(url!)/posts")
        Getmap.check(link!, token: token!, offset: nil){
            (result,ereor) in
           
            self.maplocation = result!
            for tmp in self.maplocation{
                if tmp.location!.latitude! != nil && tmp.location!.longitude! != nil{
                var latitudemap = Double(tmp.location!.latitude!)
                var longitudemap = Double(tmp.location!.longitude!)
                
                let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitudemap!, longitudemap!)
                
                let pin = customPin(pinTitle: (tmp.location?.title)!, pinSubTitle: "", location: location)
                    self.mapView.addAnnotation(pin)}
            }
        }
        
//        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(16.058994, 103.669589)
////        annotation.coordinate = location
////        annotation.title = "HOME"
////
////
//      
//
//        let pin = customPin(pinTitle: "Home", pinSubTitle: "", location: location)
//          mapView.addAnnotation(pin)
//        mapView.delegate = self
        if mode == 1{
            //self.navigationItem.rightBarButtonItem = nil
            self.navigation.isHidden = true
            mapView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
            
        }

    }
    @IBAction func saveBtn(_ sender: Any) {
        print("latitude : \(latitude!)","longitude : \(longitude!)")
    }
    @IBAction func cancelBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "cancelsegue", sender: self)

    }
    
    @IBAction func currentBtn(_ sender: Any) {
        let userlocation = mapView.userLocation
        let region = MKCoordinateRegion(center: (userlocation.location?.coordinate)!, latitudinalMeters: 400, longitudinalMeters: 400)
        
        mapView.setRegion(region, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cancelsegue"{
        let maintabbar = segue.destination as! MaintabbarView
        maintabbar.mode = self.mode
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        GoToCenterLocation()
        let firstlocation = locations.first
        self.latitude = firstlocation?.coordinate.latitude
        self.longitude = firstlocation?.coordinate.longitude
        
    }
    func GoToCenterLocation() {
        if let locMan = mapLocationManager.location {
            let region = MKCoordinateRegion(center: locMan.coordinate, latitudinalMeters: 400, longitudinalMeters: 400)
            mapView.setRegion(region, animated: true)
        }
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? customPin else { return nil }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            let smallSquare = CGSize(width: 30, height: 30)
            let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
             button.setBackgroundImage(UIImage(named: "car"), for: .normal)

            view.calloutOffset = CGPoint(x: -5, y: 5)
            //view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            view.leftCalloutAccessoryView = button

        }
        return view
        
    
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! customPin
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
//
//    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?{
//
//        let reuseId = "pin"
//        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKMarkerAnnotationView
//        pinView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//        pinView?.glyphTintColor = UIColor.orange
//        pinView?.canShowCallout = true
//
//        button.addTarget(self, action: "getDirections", for: .touchUpInside)
//        return pinView
//    }

}
