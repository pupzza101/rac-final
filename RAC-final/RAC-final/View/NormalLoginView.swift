//
//  NormalLoginView.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class NormalLoginView: UIViewController {

    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginBt: UIButton!
    
    var token:String?
    var info:ModelUserInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.navigationItem.title = "เข้าสู่ระบบ"
        loginBt.addTarget(self, action: #selector(login), for: .touchUpInside)
        loginBt.layer.cornerRadius = 5
        loginBt.layer.borderColor = UIColor.white.cgColor
        loginBt.layer.borderWidth = 0.5
        loginBt.clipsToBounds = true
        
        usernameText.layer.cornerRadius = 5
        usernameText.clipsToBounds = true
        
        passwordText.layer.cornerRadius = 5
        passwordText.clipsToBounds = true
        self.view.backgroundColor = UIColor(red:0.96, green:0.36, blue:0.22, alpha:1.0)

    }
    
    @objc func login() {
        let link = URL(string:"\(url!)/login")
        Checklogin.check(link!, username: usernameText.text!, password: passwordText.text!){(result,error) in
            
            self.token = result?.token
            self.info = result?.info
            if self.token != nil{
                
               self.performSegue(withIdentifier: "loginsegue", sender: self)
                
                UserDefaults.standard.savetoken(token: self.token!)
                print("token : \(self.token!)")
            
                UserDefaults.standard.saveinfo(username: self.info?.username, name: self.info?.name, email: self.info?.email, position: self.info?.position, tel: self.info?.phone, image: self.info?.image, role: self.info?.role, user_id: self.info?.user_id, gender: self.info?.gender, address: self.info?.address,verify:self.info?.verify)
                
            }else{
                let alert = UIAlertController(title: "เข้าสู่ระบบ ไม่สำเร็จ", message: nil, preferredStyle: .alert)
                
                
                alert.addAction(UIAlertAction(title: "ตกลง", style: .cancel, handler: nil ))
                self.present(alert, animated: true)

            }
       }
       //

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let maintabbar = segue.destination as? MaintabbarView{
            maintabbar.str = usernameText.text
        }
    }
}
