//
//  EditpostView.swift
//  RAC-final
//
//  Created by MSU IT CS on 19/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DropDown
import DLRadioButton
import ImagePicker

class EditpostView: UIViewController {

    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var typedropdownBt: UIButton!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var RdlBt: DLRadioButton!
    @IBOutlet weak var Rdl2Bt: DLRadioButton!
    @IBOutlet weak var collectionview: UICollectionView!
    
    var images = [UIImage]()
    var allimage = [UIImage]()
    var type:String?
    let dropdown = DropDown()
    var subtype:String? = ""
    var latitude:String?
    var longitude:String?
    var mapinfo:Getmap?
    var favinfo:GetFavpost?
    let imagePickerController = ImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        collectionview.delegate = self
        collectionview.dataSource = self
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 5
        
        if favinfo == nil{
            if mapinfo?.nature != nil{
                self.typedropdownBt.setTitle(self.mapinfo?.nature!, for: .normal)
                
            }else{
                self.typedropdownBt.setTitle("เลือกอาการ", for: .normal)
                
            }
            titleText.text = self.mapinfo?.title!
            descriptionText.text = self.mapinfo?.description!
            
            if mapinfo?.type_post == "Accident"{
                RdlBt.isSelected = true
                self.type = "Accident"
                typedropdownBt.isHidden = false
                dropdown.anchorView = typedropdownBt
                dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
                dropdown.dataSource = [ "หมดสติ", "อาการชัก","บาดเจ็บที่ศีรษะ","กระดูกหัก"]
                
                typedropdownBt.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
                dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                    self.typedropdownBt.setTitle(item, for: .normal)
                    
                    self.subtype = item
                }
                
                typedropdownBt.layer.cornerRadius = 10
                typedropdownBt.clipsToBounds = true
                typedropdownBt.layer.borderWidth = 0.125
            }else{
                Rdl2Bt.isSelected = true
                self.type = "Crime"
                
            }
            if self.mapinfo?.images.count != 0{
                for tmp in (self.mapinfo?.images)!{
                    let raw = "\(url!)\(tmp.image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.images.append(UIImage(data:data)!)
                    }
                }
                
            }else{
                self.images = [UIImage(named:"noimage")!]
                
                
            }
        }else{
            if favinfo!.post!.nature! != nil{
                self.typedropdownBt.setTitle(self.favinfo?.post!.nature!, for: .normal)
                
            }else{
                self.typedropdownBt.setTitle("เลือกอาการ", for: .normal)
                
            }
            titleText.text = self.favinfo!.post!.location!.title!
            descriptionText.text = self.favinfo?.post!.description!
            
            if favinfo!.post!.type_post! == "Accident"{
                RdlBt.isSelected = true
                self.type = "Accident"
                typedropdownBt.isHidden = false
                dropdown.anchorView = typedropdownBt
                dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
                dropdown.dataSource = [ "หมดสติ", "อาการชัก","บาดเจ็บที่ศีรษะ","กระดูกหัก"]
                
                typedropdownBt.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
                dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                    self.typedropdownBt.setTitle(item, for: .normal)
                    
                    self.subtype = item
                }
                
                typedropdownBt.layer.cornerRadius = 10
                typedropdownBt.clipsToBounds = true
                typedropdownBt.layer.borderWidth = 0.125

            }else{
                Rdl2Bt.isSelected = true
                self.type = "Crime"
                
            }
            if self.favinfo?.post!.images.count != 0{
                for tmp in (self.favinfo!.post!.images){
                    let raw = "\(url!)\(tmp.image!)"
                    let  Nsurl = NSURL(string: raw )!
                    
                    if let data = try? Data(contentsOf: Nsurl as URL)
                    {
                        //  images.append(UIImage(data: data)!)
                        self.images.append(UIImage(data:data)!)
                    }
                }
                
            }else{
                self.images = [UIImage(named:"noimage")!]
                
                
            }
        }
     
     
        
        let doneBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(updatepost), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton
        
   

    }
    override func viewWillAppear(_ animated: Bool) {
        latitude = UserDefaults.standard.getlatitude()
        longitude = UserDefaults.standard.getlongtitude()
        
    }
    
    @objc func updatepost(){
        
       // let link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")
        let link:URL
        if favinfo == nil{
            link = URL(string:"\(url!)/posts/\(self.mapinfo!.id!)")!
        }else{
            link = URL(string:"\(url!)/posts/\(self.favinfo!.post!.post_id!)")!
            
        }
        var imageBase64 = [String]()
        if images.count != 0{
            for i in images{
                let imgData = i.jpegData(compressionQuality: 0.5)
                imageBase64.append((imgData?.base64EncodedString())!)
                // imageBase64.append(convertImageTobase64(format: .jpeg(1.0), image: i)!)
            }
        }
      
        
        Updatepost.update(link, token: token, title: titleText.text!, description: descriptionText.text!, status: self.mapinfo?.status!, type_post: type!, nature: self.subtype, image: imageBase64, titlelocation: titleText.text!, latitude: latitude!, longtitude: longitude!){
            (result,error) in
            
            if result != nil{
                
                self.navigationController?.popToRootViewController(animated: true)

            }
            
        }
        
        
        
    }
    
    @IBAction func importimage(_ sender: Any) {
        present(imagePickerController, animated: true, completion: nil)

    }
    
    @IBAction func RdlAction(_ sender: DLRadioButton) {
        
        if sender.tag == 1{
            print("1")
            type = "Accident"
            typedropdownBt.isHidden = false
            dropdown.anchorView = typedropdownBt
            dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
            dropdown.dataSource = [ "หมดสติ", "อาการชัก","บาดเจ็บที่ศีรษะ","กระดูกหัก"]
            
            typedropdownBt.addTarget(self, action: #selector(dropdownAction), for: .touchUpInside)
            dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.typedropdownBt.setTitle(item, for: .normal)
                
                self.subtype = item
            }
            
            typedropdownBt.layer.cornerRadius = 10
            typedropdownBt.clipsToBounds = true
            typedropdownBt.layer.borderWidth = 0.125
            
            
            
        }else{
            print("2")
            typedropdownBt.isHidden = true
            type = "Crime"
        }
    }
    
    
    @objc func dropdownAction(){
        dropdown.show()
        
    }

}
extension EditpostView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
        print("++")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        allimage.removeAll()
        allimage += images
        
        
        imagePicker.dismiss(animated: true){
            
            self.images += self.allimage
            
            DispatchQueue.main.async {
                self.collectionview.reloadData()
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
extension EditpostView:UICollectionViewDataSource,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageUpdateCollectionCell
        
        cell.imageview.image = images[indexPath.row]
        cell.tmp = {
            self.images.remove(at: indexPath.row)
            self.collectionview.reloadData()
        }
        
        return cell
        
    }
    
    
}
