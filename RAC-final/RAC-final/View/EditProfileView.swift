//
//  EditProfileView.swift
//  RAC-final
//
//  Created by MSU IT CS on 19/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DLRadioButton
import ImagePicker
class EditProfileView: UIViewController {

    
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var telText: UITextField!
    @IBOutlet weak var addressText: UITextField!
    @IBOutlet weak var RdlBt: DLRadioButton!
    @IBOutlet weak var Rdl2Bt: DLRadioButton!
    @IBOutlet weak var changeBy: UIButton!
    
    var name:String?
    var email:String?
    var tel:String?
    var image:UIImage?
    var address:String?
    var gender:String?
    var imgBase64:String?
    var role:String?

    var allimage = [UIImage]()
    let imagePickerController = ImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "แก้ไขข้อมูลส่วนตัว"
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        nameText.text = name
        emailText.text = email
        telText.text = tel
        addressText.text = address
        imageview.layer.cornerRadius = imageview.frame.height/2
        imageview.clipsToBounds = true
        if image != nil{
            imageview.image = image
            
        }else{
            imageview.image = UIImage(named: "photo")
        }
        if gender == "Male"{
            RdlBt.isSelected = true
        }else{
            Rdl2Bt.isSelected = true
        }
        role = UserDefaults.standard.getrole()

        if role! == "2"{
          
              changeBy.isHidden = true
            
        }else if role! == "3"
        {
          
             changeBy.isHidden = true
            
            
        }else{
           changeBy.isHidden = false
        }
        // Do any additional setup after loading the view.
        let doneBtn = UIButton(type: .custom)
        // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(edit), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton
        self.view.backgroundColor = UIColor(red:0.96, green:0.36, blue:0.22, alpha:1.0)

    }
    @IBAction func importimageBtn(_ sender: Any) {
        present(imagePickerController, animated: true, completion: nil)

    }
    
    @IBAction func RdlAction(_ sender: DLRadioButton) {
        if sender.tag == 1{
            print("Male")
            gender = "Male"
        }else if sender.tag == 2{
            print("Female")
            gender = "Female"
        }
        
    }
    @objc func edit(){
        
        let link = URL(string:"\(url!)/users")
        let user_id = UserDefaults.standard.getuser_id()
        if allimage.count != 0{
            let imgData = allimage[0].jpegData(compressionQuality: 0.5)
            imgBase64 = imgData?.base64EncodedString()
        }else if self.image != nil{
            let imgData = self.image!.jpegData(compressionQuality: 0.5)
            imgBase64 = imgData?.base64EncodedString()
        }
        
        UpdateProfile.update(link!,user_id:user_id!, name: nameText.text!, email: emailText.text!, tel: telText.text!, image: imgBase64!, gender: gender!, address: addressText.text!, verify: nil, verify_image: nil){
            (result,error) in
            
            if error == nil{
                self.navigationController?.popViewController(animated: true)
            }else{
                print(error)
            }

        }
        
    }
  

}
extension EditProfileView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
        print("++")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        allimage.removeAll()
        allimage += images
        
        
        imagePicker.dismiss(animated: true){
            
            if self.allimage.count != 0{
                self.imageview.image = self.allimage[0]
                
            }
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
