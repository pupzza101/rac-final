//
//  RegisterView.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit
import DLRadioButton
import ImagePicker


class RegisterView: UIViewController {

    @IBOutlet weak var radioBt: DLRadioButton!
    @IBOutlet weak var radioBt2: DLRadioButton!
    @IBOutlet weak var imageview: UIImageView!
    var topic:String = "สมัครสมาชิก"
    var allimage = [UIImage]()
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var telText: UITextField!
    @IBOutlet weak var addressText: UITextField!
    @IBOutlet weak var importimageBt: UIButton!
    
    var username:String?
    var name:String?
    var email:String?
    var tel:String?
    var image:UIImage?
    var address:String?
    var gender:String?
    var mode:Int? = 1
    var imgBase64:String?
    
    let imagePickerController = ImagePickerController()
   
    override func viewDidLoad() {
        super.viewDidLoad()
         imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        self.view.backgroundColor = UIColor(red:0.96, green:0.36, blue:0.22, alpha:1.0)

        self.tabBarController?.tabBar.isHidden = true

            self.hideKeyboardWhenTappedAround()
        self.navigationItem.title = topic
        self.setcorner(usernameText)
        self.setcorner(passwordText)
        self.setcorner(nameText)
        self.setcorner(emailText)
        self.setcorner(telText)
        self.setcorner(addressText)
        
        passwordText.text = username
        nameText.text = name
        emailText.text = email
        telText.text = tel
        addressText.text = address
        if image != nil{
            imageview.image = image

        }else{
            imageview.image = UIImage(named: "photo")
        }
       
        if mode != 1{
            usernameText.isHidden = true
            passwordText.isHidden = true
            
            if gender == "Male"{
                radioBt.isSelected = true
            }else{
                radioBt2.isSelected = true
            }
        }
        let doneBtn = UIButton(type: .custom)
       // doneBtn.setImage(UIImage(named: "add-group"), for: .normal)
        doneBtn.addTarget(self, action: #selector(register), for: .touchUpInside)
        doneBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        doneBtn.setTitle("ยืนยัน", for: .normal)
        doneBtn.setTitleColor(UIColor.blue, for: .normal)
        doneBtn.clipsToBounds = true
        
        let barButton = UIBarButtonItem(customView: doneBtn)
        
        self.navigationItem.rightBarButtonItem = barButton

        
    }
   
    @objc func register(){
        if allimage.count != 0{
        let imgData = allimage[0].jpegData(compressionQuality: 0.5)
         imgBase64 = imgData?.base64EncodedString()
        }
        if mode == 1{
            if usernameText.text! != "" && passwordText.text! != "" && nameText.text! != "" && emailText.text! != "" && telText.text! != "" && addressText.text! != ""{
                
                let link = URL(string:"\(url!)/users")
                
                Register.create(link!, username: usernameText.text!, password: passwordText.text!, name: nameText.text!, email: emailText.text!, tel: telText.text!, image: imgBase64, gender: gender!, address: addressText.text!){
                    (result,error) in
                    
                    if result?.message! != "Same username or email"{
                        let alert = UIAlertController(title: "สมัครสมาชิก สำเร็จ!! ", message: "", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: {
                            action in
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                        
                        
                        self.present(alert, animated: true)
                        
                    }else if result?.message! == "Same username or email"{
                        let alert = UIAlertController(title: "Usernanme/Email มีผู้ใช้แล้ว! ", message: "", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: {
                            action in
                            
                        }))
                        
                        
                        self.present(alert, animated: true)
                    }
                    else{
                        let alert = UIAlertController(title: "สมัครสมาชิก ไม่สำเร็จ", message: "", preferredStyle: .alert)
                        
                        
                        alert.addAction(UIAlertAction(title: "ตกลง", style: .cancel, handler: nil))
                        
                        self.present(alert, animated: true)
                    }
                }
            }else{
                let alert = UIAlertController(title: "กรุณากรอกข้อมูลให้ครบถ้วน", message: "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: {
                    action in
                   
                    
                }))
                
                
                self.present(alert, animated: true)
            }
    
        }else{
         
        }
    }
     func setcorner(_ textfield: UITextField) {
        
        textfield.clipsToBounds = true
        textfield.layer.cornerRadius = 5
    }
    
    @IBAction func RadioBtn(_ sender: DLRadioButton) {
        
        if sender.tag == 1{
            print("Male")
            gender = "Male"
        }else if sender.tag == 2{
            print("Female")
            gender = "Female"
        }
    }
    
    @IBAction func importimage(_ sender: Any) {
        present(imagePickerController, animated: true, completion: nil)

    }
    
    
    
}
extension RegisterView:ImagePickerDelegate{
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else {return}
        print("++")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        allimage.removeAll()
        allimage += images
        

        imagePicker.dismiss(animated: true){
            
            if self.allimage.count != 0{
            self.imageview.image = self.allimage[0]
               
            }
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
}
