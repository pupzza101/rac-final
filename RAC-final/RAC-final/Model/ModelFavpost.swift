//
//  File.swift
//  RAC-final
//
//  Created by MSU IT CS on 17/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelFavpost:Mappable{
    
    var post_id:Int?
    var title:String?
    var description:String?
    var type_post:String?
    var status:String?
    var nature:String?
    var images = [ModelImagepost]()
    var location:ModelLocation?
    var create:ModelCreaterpost?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        post_id <- map["post_id"]
        title <- map["title"]
        description <- map["description"]
        type_post <- map["type_post"]
        status <- map["status"]
        nature <- map["nature"]
        images <- map["image"]
        location <- map["location"]
        create <- map["creater"]


    }
    
    
    
    
}
