//
//  ModelImagepost.swift
//  RAC-final
//
//  Created by MSU IT CS on 16/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelImagepost:Mappable{
    
    var id:String?
    var image:String?
   
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
         id <- map["id"]
        image <- map["image"]
    }
    
    
    
    
}
