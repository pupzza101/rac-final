

import ObjectMapper


class ModelUserInfo:Mappable{
    
    var username:String?
    var name:String?
    var email:String?
    var position:String?
    var phone:String?
    var gender:String?
    var address:String?
    var image:String?
    var role:Int?
    var user_id:Int?
    var verify:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        username <- map["username"]
        name <- map["name"]
        email <- map["email"]
        position <- map["position"]
        address <- map["address"]
        gender <- map["gender"]
        phone <- map["phone"]
        image <- map["image"]
        role <- map["role_id"]
        user_id <- map["id"]
        verify <- map["verify"]
    }
    
    
    
}
