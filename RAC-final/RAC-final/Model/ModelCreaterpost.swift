//
//  ModelCreaterpost.swift
//  RAC-final
//
//  Created by MSU IT CS on 17/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import Foundation
import ObjectMapper

class ModelCreaterpost:Mappable{
    
    var user_id:Int?
    var name:String?
    var role_id:Int?
    var image:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        user_id <- map["user_id"]
        name <- map["name"]
        role_id <- map["role_id"]
        image <- map["image"]
    }
    
    
    
    
}
