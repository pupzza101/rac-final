//
//  DetaileCell.swift
//  RAC-final
//
//  Created by MSU IT CS on 20/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class DetaileCell: UITableViewCell {

    
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var jobText: UILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var userimageView: UIImageView!
    @IBOutlet weak var statusText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
