//
//  ImageCollectionCell.swift
//  RAC-final
//
//  Created by MSU IT CS on 20/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
