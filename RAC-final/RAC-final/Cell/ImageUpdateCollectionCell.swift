//
//  ImageUpdateCollectionCell.swift
//  RAC-final
//
//  Created by MSU IT CS on 19/6/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class ImageUpdateCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
    var tmp: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func deleteBtn(_ sender: Any) {
        tmp?()

    }
}
