//
//  FirstpageCell.swift
//  RAC-final
//
//  Created by MSU IT CS on 14/5/2562 BE.
//  Copyright © 2562 MSU IT CS. All rights reserved.
//

import UIKit

class FirstpageCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var statusimageview: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
